import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from "react-hook-form";

import { Button, Col, Container, Form, Row, Stack } from "react-bootstrap";

import AmenitiesApi from "../api/AmenitiesApi";
import { createProperty, storeProperty } from "../redux/actionCreators";

const propertyTypes = {
  HOUSE: "Casa",
  APARTAMENT: "Departamento",
  TERRAIN: "Terreno",
  OFFICE: "Oficina",
  LOCAL: "Local comercial",
};

const operations = {
  SALE: "Venta",
  RENT: "Renta",
  TRANSFER: "Traspaso",
};

const formStateToPayload = (formState) => {
  const {
    publicKey,
    name,
    description,
    price,
    propertyType,
    operation,
    state,
    city,
    neighborhood,
    cp,
    street,
    latitude,
    longitude,
    numBathrooms,
    bedrooms,
    m2Construction,
    parking,
    age,
    departments,
    floor,
  } = formState;
  const amenities = Object.getOwnPropertyNames(formState)
    .filter(
      (amenityFormId) =>
        amenityFormId.startsWith("formAmenities-") && formState[amenityFormId]
    )
    .map((amenityFormId) => amenityFormId.split("formAmenities-")[1]);

  return {
    property: {
      publicKey,
      name,
      description,
      price: Number(price),
      propertyType,
      operation,
      state,
      city,
      neighborhood,
      cp,
      street,
      latitude: Number(latitude),
      longitude: Number(longitude),
      numBathrooms: Number(numBathrooms),
      bedrooms: Number(bedrooms),
      m2Construction: Number(m2Construction),
      parking: Number(parking),
      age: Number(age),
      departments: Number(departments),
      floor: Number(floor),
    },
    amenities,
  };
};

const NewProperty = () => {
  const dispatch = useDispatch();
  const [isLoading, setIsLoading] = useState(false);
  const [amenitiesCatalog, setAmenitiesCatalog] = useState([]);
  const {
    isSubmitting,
    formValues: {
      property: {
        publicKey,
        name,
        description,
        price,
        propertyType,
        operation,
        state,
        city,
        neighborhood,
        cp,
        street,
        latitude,
        longitude,
        numBathrooms,
        bedrooms,
        m2Construction,
        parking,
        age,
        departments,
        floor,
      },
      images,
      amenities,
    },
  } = useSelector(({ createPropertyFormReducer }) => createPropertyFormReducer);

  // fetch amenitieas at mount
  useEffect(() => {
    let isMounted = true;
    setIsLoading(true);
    AmenitiesApi.getAllAmenities()
      .then((resp) => {
        if (isMounted) setAmenitiesCatalog(resp.data);
      })
      .catch((err) => {
        if (isMounted) setAmenitiesCatalog([]);
        console.error("Error fetching amenities", err);
      })
      .finally(() => setIsLoading(false));
    return () => {
      isMounted = false;
    };
  }, []);

  const {
    handleSubmit,
    control,
    formState: { errors },
    getValues,
    trigger,
  } = useForm({ mode: "onChange" });

  const onSessionSave = () => {
    trigger().then((isValid) => {
      if (!isValid) return;
      const formValues = getValues();
      const payload = formStateToPayload(formValues);
      dispatch(storeProperty(payload));
    });
  };

  const onSubmit = (data) => {
    const payload = formStateToPayload(data);
    dispatch(createProperty(payload));
  };

  const isFormDisabled = isLoading || isSubmitting;

  return (
    <Container className="m-1 m-lg-2 m-xl-4 mb-5">
      <h1>Nueva propiedad</h1>

      <Form className="w-75" onSubmit={handleSubmit(onSubmit)}>
        <Form.Group className="mb-3" controlId="formPublicKey">
          <Form.Label>Public key</Form.Label>
          <Controller
            name="publicKey"
            defaultValue={publicKey}
            rules={{ required: true }}
            control={control}
            render={({ field }) => (
              <Form.Control type="text" {...field} disabled={isFormDisabled} />
            )}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formName">
          <Form.Label>Nombre</Form.Label>
          <Controller
            name="name"
            defaultValue={name}
            rules={{ required: true }}
            control={control}
            render={({ field }) => (
              <Form.Control type="text" {...field} disabled={isFormDisabled} />
            )}
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="formDescription">
          <Form.Label>Descripción</Form.Label>
          <Controller
            name="description"
            defaultValue={description}
            rules={{ required: true }}
            control={control}
            render={({ field }) => (
              <Form.Control
                as="textarea"
                rows="2"
                {...field}
                disabled={isFormDisabled}
              />
            )}
          />
        </Form.Group>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formPrice">
            <Form.Label>Precio</Form.Label>
            <Controller
              name="price"
              defaultValue={price}
              rules={{ required: true }}
              control={control}
              render={({ field }) => <Form.Control type="number" {...field} />}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formPropertyType">
            <Form.Label>Tipo propiedad</Form.Label>
            <Controller
              name="propertyType"
              defaultValue={propertyType}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Select {...field} disabled={isFormDisabled}>
                  <option value="">Seleccione una opción</option>
                  {Object.getOwnPropertyNames(propertyTypes).map((type) => (
                    <option key={`propertyType_option_${type}`} value={type}>
                      {propertyTypes[type]}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formOperation">
            <Form.Label>Tipo transacción</Form.Label>
            <Controller
              name="operation"
              defaultValue={operation}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Select {...field} disabled={isFormDisabled}>
                  <option value="">Seleccione una opción</option>
                  {Object.getOwnPropertyNames(operations).map((type) => (
                    <option key={`operation_option_${type}`} value={type}>
                      {operations[type]}
                    </option>
                  ))}
                </Form.Select>
              )}
            />
          </Form.Group>
        </Row>

        <h5>Dirección y localización</h5>

        <Form.Group className="mb-3" controlId="formStreet">
          <Form.Label>Calle</Form.Label>
          <Controller
            name="street"
            defaultValue={street}
            rules={{ required: true }}
            control={control}
            render={({ field }) => (
              <Form.Control type="text" {...field} disabled={isFormDisabled} />
            )}
          />
        </Form.Group>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formNeighborhood">
            <Form.Label>Colonia</Form.Label>
            <Controller
              name="neighborhood"
              defaultValue={neighborhood}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formCity">
            <Form.Label>Delagación / Municipio</Form.Label>
            <Controller
              name="city"
              defaultValue={city}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formCP">
            <Form.Label>Código Postal</Form.Label>
            <Controller
              name="cp"
              defaultValue={cp}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formState">
            <Form.Label>Estado</Form.Label>
            <Controller
              name="state"
              defaultValue={state}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="text"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formLatitude">
            <Form.Label>Latitud</Form.Label>
            <Controller
              name="latitude"
              defaultValue={latitude}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formLongitude">
            <Form.Label>Longitud</Form.Label>
            <Controller
              name="longitude"
              defaultValue={longitude}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
        </Row>

        <h5>Atributos</h5>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formNumBathrooms">
            <Form.Label>Baños</Form.Label>
            <Controller
              name="numBathrooms"
              defaultValue={numBathrooms}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formBedrooms">
            <Form.Label>Habitaciones</Form.Label>
            <Controller
              name="bedrooms"
              defaultValue={bedrooms}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formM2Construction">
            <Form.Label>
              Construcción m<sup>2</sup>
            </Form.Label>
            <Controller
              name="m2Construction"
              defaultValue={m2Construction}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formParking">
            <Form.Label>Estacionamiento</Form.Label>
            <Controller
              name="parking"
              defaultValue={parking}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
        </Row>

        <Row className="mb-3">
          <Form.Group as={Col} controlId="formAge">
            <Form.Label>Edad</Form.Label>
            <Controller
              name="age"
              defaultValue={age}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formDepartments">
            <Form.Label>Departamentos</Form.Label>
            <Controller
              name="departments"
              defaultValue={departments}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Form.Group as={Col} controlId="formFloor">
            <Form.Label>Piso</Form.Label>
            <Controller
              name="floor"
              defaultValue={floor}
              rules={{ required: true }}
              control={control}
              render={({ field }) => (
                <Form.Control
                  type="number"
                  {...field}
                  disabled={isFormDisabled}
                />
              )}
            />
          </Form.Group>
          <Col>&nbsp;</Col>
        </Row>

        <h5>Imágenes</h5>

        <h5>Amenidades</h5>

        <div className="mb-3">
          {amenitiesCatalog.map((amenity) => (
            <Controller
              key={`amenity_${amenity.id}`}
              name={`formAmenities-${amenity.id}`}
              control={control}
              render={({ field }) => (
                <Form.Check
                  type="switch"
                  id={`formAmenities-${amenity.id}`}
                  label={amenity.name}
                  {...field}
                />
              )}
            />
          ))}
        </div>

        <Stack className="mb-3" direction="horizontal" gap={2}>
          <Button
            variant="primary"
            type="submit"
            name="submit"
            disabled={isFormDisabled}
          >
            Guardar registro
          </Button>
          <Button
            variant="primary"
            type="button"
            name="save"
            onClick={onSessionSave}
            disabled={isFormDisabled}
          >
            Guardar en sesión
          </Button>
          <Button variant="danger" type="button" disabled={isFormDisabled}>
            Cancelar
          </Button>
        </Stack>
      </Form>
    </Container>
  );
};

export default NewProperty;
