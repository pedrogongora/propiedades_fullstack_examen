import React, { useEffect } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Col, Row, Container, Card } from "react-bootstrap";
import { MdDeleteOutline, MdEdit, MdVisibility } from "react-icons/md";

import PropertiesMap from "../Components/PropertiesMap";

import store from "../redux/store";
import { getAllProperties } from "../redux/actionCreators";

const PropertyGridItemImage = ({ src }) => (
  <div
    className="PropertyGridItemImage"
    style={{
      background: `url("${src}")  center/cover no-repeat`,
    }}
  />
);

const PropertyGridItem = ({ property }) => (
  <Card className="shadow m-1 m-lg-2 m-xl-4">
    <PropertyGridItemImage src={(property.images[0] || { path: "" }).path} />
    <Card.Body>
      <Container>
        <Row>
          <Col className="text-center">
            <Link to={`/property/${property.id}`}>
              <MdVisibility size={"1.5rem"} />
            </Link>
          </Col>
          <Col className="text-center">
            <MdEdit size={"1.5rem"} />
          </Col>
          <Col className="text-center">
            <MdDeleteOutline size={"1.5rem"} />
          </Col>
        </Row>
      </Container>
    </Card.Body>
  </Card>
);

const PropertiesGrid = ({ properties }) => (
  <Row md={3} sm={2} xs={1}>
    {properties.map((property) => (
      <Col key={`grid_item_layout_${property.id}`}>
        <PropertyGridItem property={property} />
      </Col>
    ))}
  </Row>
);

const HomePage = ({ properties }) => {
  useEffect(() => {
    store.dispatch(getAllProperties());
  }, []);

  return (
    <div>
      <div>
        {properties && <PropertiesMap properties={properties} />}
      </div>
      <Container>
        {properties ? <PropertiesGrid properties={properties} /> : <div />}
      </Container>
    </div>
  );
};

const mapStateToProps = (state) => ({
  properties: state.propertyReducer.properties,
});

export default connect(mapStateToProps, {})(HomePage);
