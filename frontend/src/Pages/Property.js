import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import { Card, Col, Container, Row, Stack } from "react-bootstrap";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import {
  getProperty,
  getPropertyAmenities,
  getPropertyImages,
} from "../redux/actionCreators";
import {
  MdApartment,
  MdAspectRatio,
  MdBed,
  MdCheckBox,
  MdDateRange,
  MdLocalParking,
  MdShower,
} from "react-icons/md";

const propertyTypes = {
  HOUSE: "Casa",
  APARTAMENT: "Departamento",
  TERRAIN: "Terreno",
  OFFICE: "Oficina",
  LOCAL: "Local comercial",
};

const operations = {
  SALE: "Venta",
  RENT: "Renta",
  TRANSFER: "Traspaso",
};

const FrontMatter = ({ property, images }) => {
  const coverSrc = images && images.length ? images[0].path : "";
  const containerStyle = {
    height: "40vh",
    minHeight: "40vh",
    background: `url("${coverSrc}") center/cover no-repeat`,
    textShadow: "2px 2px 2px rgba(0, 0, 0, 0.2)",
  };
  return (
    <Container
      style={containerStyle}
      className="text-dark p-1 p-md-2 p-lg-4 mb-1 mb-lg-2 mb-xl-4"
    >
      <div className="d-table h-100 align-bottom">
        <div className="d-table-cell h-100 align-bottom">
          <div>
            {propertyTypes[property.property_type]} en{" "}
            {operations[property.operation]}
          </div>
          <div className="display-3">{property.street}</div>
          {property.neighborhood}, {property.city}, {property.state}
          <br />
          <div className="display-4">{property.price}</div>
        </div>
      </div>
    </Container>
  );
};

const Gallery = ({ property, images }) => {
  const [currentImage, setCurrentImage] = useState(0);
  const settings = {
    autoplay: true,
    adaptiveHeight: true,
    arrows: true,
    centerMode: true,
    className: "",
    dots: false,
    infinite: true,
    slidesToShow: 1,
    variableWidth: true,
    afterChange: setCurrentImage,
  };

  return (
    <div className="mb-1 mb-lg-2 mb-xl-4">
      <Slider {...settings}>
        {(images ?? []).map(({ id, path }) => (
          <img
            key={`property_${property?.id}gallery_img_${id}`}
            src={path}
            alt="imagen propiedad"
          />
        ))}
      </Slider>
      <div className="small pt-2">{`${currentImage + 1} / ${
        (images ?? []).length
      }`}</div>
    </div>
  );
};

const Details = ({ property, amenities }) => {
  const DetailCard = ({ icon, description, value }) => (
    <Card
      className="border-0"
      style={{ width: "10rem", maxWidth: "10rem", backgroundColor: "initial" }}
    >
      <Card.Body className="text-center">
        {icon}
        <div className="pt-1 pb-1 small">{description}</div>
        <div>{value}</div>
      </Card.Body>
    </Card>
  );

  return (
    <div className="border p-1 p-lg-2 p-xl-4">
      <div className="h2">Detalles del inmueble</div>
      <div className="mb-1 mb-lg-2 mb-xl-4">
        <p>{property?.name}</p>
        <p>{property?.description}</p>
        <p>
          {property?.neighborhood}, {property?.city}, {property?.state}.
        </p>
        <p className="h4">{property?.price}</p>
      </div>
      <div className="h3">Características del inmueble</div>
      <Container className="mb-1 mb-lg-2 mb-xl-4">
        <Row md={"auto"}>
          <Col>
            <DetailCard
              icon={<MdBed />}
              description="Recámaras"
              value={property?.bedrooms}
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdShower />}
              description="Baños"
              value={property?.num_bathrooms}
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdAspectRatio />}
              description="Tamaño de construcción"
              value={
                <>
                  {property?.m2_construction} m<sup>2</sup>
                </>
              }
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdLocalParking />}
              description="Estacionamiento"
              value={property?.parking}
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdDateRange />}
              description="Edad"
              value={property?.age}
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdApartment />}
              description="Número de departamentos"
              value={property?.departments}
            />
          </Col>
          <Col>
            <DetailCard
              icon={<MdApartment />}
              description="Piso"
              value={property?.floor}
            />
          </Col>
        </Row>
      </Container>
      {(amenities ?? []).length ? (
        <>
          <div className="h3">Amenidades</div>
          <Stack gap={3}>
            {(amenities ?? []).map(({ name }) => (
              <Stack direction="horizontal">
                <MdCheckBox />
                &nbsp;&nbsp;
                {name}
              </Stack>
            ))}
          </Stack>
        </>
      ) : null}
    </div>
  );
};

const Property = () => {
  const dispatch = useDispatch();
  const { propertyId } = useParams();
  const {
    currentProperty: property,
    currentPropertyImages: images,
    currentPropertyAmenities: amenities,
  } = useSelector(({ propertyReducer }) => propertyReducer);

  useEffect(() => {
    dispatch(getProperty(propertyId));
    dispatch(getPropertyImages(propertyId));
    dispatch(getPropertyAmenities(propertyId));
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [propertyId]);

  return (
    <Container className="m-1 m-lg-2 m-xl-4 mb-5">
      {property && (
        <>
          <FrontMatter property={property} images={images} />
          <Gallery property={property} images={images} />
          <Details property={property} amenities={amenities} />
        </>
      )}
    </Container>
  );
};

export default Property;
