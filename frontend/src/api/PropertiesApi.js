import Axios from "axios";

const API_URL = process.env.REACT_APP_API_URL;

const PropertiesApi = {
  getAllProperties: () => Axios.get(`${API_URL}/properties`),
  getProperty: (propertyId) =>
    Axios.get(`${API_URL}/properties/property/${propertyId}`),
  getPropertyImages: (propertyId) =>
    Axios.get(`${API_URL}/properties/images/${propertyId}`),
  getPropertyAmenities: (propertyId) =>
    Axios.get(`${API_URL}/properties/amenities/${propertyId}`),
  createProperty: (payload) => Axios.post(`${API_URL}/properties`, payload),
};

export default PropertiesApi;
