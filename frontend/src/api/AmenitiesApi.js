import Axios from "axios";

const API_URL = process.env.REACT_APP_API_URL;

const AmenitiesApi = {
  getAllAmenities: () => Axios.get(`${API_URL}/amenities`),
};

export default AmenitiesApi;
