import React, { PureComponent } from "react";
import { Navbar, Nav, Container } from "react-bootstrap";
import { MdEdit, MdDelete } from "react-icons/md";

const { Toggle, Brand } = Navbar;
const NavLink = Nav.Link;

class NavigationBar extends PureComponent {
  closeSession = () => {
    localStorage.clear();
    window.location = "/";
  };

  render() {
    return (
      <Navbar
        collapseOnSelect
        sticky="top"
        expand="sm"
        bg="dark"
        variant="dark"
      >
        <Container>
          <Brand>
            <NavLink href="/">Habi</NavLink>{" "}
          </Brand>
          <Toggle aria-controls="responsive-navbar-nav" />
          <Nav id="responsive-navbar-nav" className="justify-content-end">
            <Nav.Item>
              <Nav.Link href={"/property/new"}>
                <MdEdit color="white" size={'2rem'} />
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link>
                <MdDelete color="white" size={'2rem'} />
              </Nav.Link>
            </Nav.Item>
            <Nav.Item>
              <Nav.Link>Log in / Log out</Nav.Link>
            </Nav.Item>
          </Nav>
        </Container>
      </Navbar>
    );
  }
}

export default NavigationBar;
