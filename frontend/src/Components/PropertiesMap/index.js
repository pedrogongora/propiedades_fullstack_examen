import React from "react";
import {
  GoogleMap,
  Marker,
  MarkerClusterer,
  useLoadScript,
} from "@react-google-maps/api";

const PropertiesMap = ({ properties }) => {
  const apiKey = process.env.REACT_APP_API_KEY_GOOGLE_MAPS;
  const containerStyle = {
    width: "100%",
    height: "400px",
  };
  const locations = properties.map(({ latitude, longitude }) => ({
    lat: Number(latitude),
    lng: Number(longitude),
  }));

  const { isLoaded, loadError } = useLoadScript({
    googleMapsApiKey: apiKey,
  });

  if (loadError) {
    console.error("Error loading map", loadError);
  }

  const onLoad = React.useCallback(
    function onLoad(mapInstance) {
      if (locations.length > 0) {
        const bounds = new window.google.maps.LatLngBounds();
        locations.forEach((location) => {
          bounds.extend(location);
        });
        mapInstance.fitBounds(bounds);
      }
    },
    [locations]
  );

  const renderMap = () => (
    <GoogleMap
      mapContainerStyle={containerStyle}
      zoom={7}
      center={
        locations[0] || {
          lat: 19.435718,
          lng: -99.144112,
        }
      }
      onLoad={onLoad}
    >
      <MarkerClusterer>
        {(clusterer) =>
          locations.map((location, i) => (
            <Marker
              key={`marker_${location}_${i}`}
              position={location}
              clusterer={clusterer}
            />
          ))
        }
      </MarkerClusterer>
    </GoogleMap>
  );

  return (
    <div className="m-1 m-lg-2 m-xl-4">{isLoaded ? renderMap() : null}</div>
  );
};

export default React.memo(PropertiesMap);
