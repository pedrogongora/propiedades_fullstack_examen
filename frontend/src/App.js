import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Container } from "react-bootstrap";
import NavigationBar from "./Components/Navbar";
import HomePage from "./Pages/Home";
import Page404 from "./Pages/Page404";
import Property from "./Pages/Property";
import NewProperty from "./Pages/NewProperty";

const App = () => {
  return (
    <div>
      <NavigationBar />
      <Container>
        <BrowserRouter>
          <Routes>
            <Route element={<NewProperty />} path="/property/new" />
            <Route element={<Property />} path="/property/:propertyId" />
            <Route element={<HomePage />} path="/" />
            <Route path="*" element={<Page404 />} />
          </Routes>
        </BrowserRouter>
      </Container>
    </div>
  );
};

export default App;
