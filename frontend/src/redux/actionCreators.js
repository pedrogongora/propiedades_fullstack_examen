import PropertiesApi from "../api/PropertiesApi";
import {
  CREATE_PROPERTY,
  GET_ALL_PROPERTIES,
  GET_PROPERTY,
  GET_PROPERTY_AMENITIES,
  GET_PROPERTY_IMAGES,
  STORE_PROPERTY,
} from "./actions";

export const getAllProperties = () => (dispatch) =>
  PropertiesApi.getAllProperties().then((resp) =>
    dispatch({
      type: GET_ALL_PROPERTIES,
      properties: resp.data,
    })
  );

export const getProperty = (propertyId) => (dispatch) =>
  PropertiesApi.getProperty(propertyId).then((resp) =>
    dispatch({
      type: GET_PROPERTY,
      property: resp.data,
    })
  );

export const getPropertyImages = (propertyId) => (dispatch) =>
  PropertiesApi.getPropertyImages(propertyId).then((resp) =>
    dispatch({
      type: GET_PROPERTY_IMAGES,
      images: resp.data,
    })
  );

export const getPropertyAmenities = (propertyId) => (dispatch) =>
  PropertiesApi.getPropertyAmenities(propertyId).then((resp) =>
    dispatch({
      type: GET_PROPERTY_AMENITIES,
      amenities: resp.data,
    })
  );

export const createProperty =
  ({ property, images, amenities }) =>
  (dispatch) =>
    PropertiesApi.createProperty({ property, images, amenities }).then((resp) =>
      dispatch({
        type: CREATE_PROPERTY,
        property,
        images,
        amenities,
      })
    );

export const storeProperty =
  ({ property, images, amenities }) =>
  (dispatch) =>
    dispatch({
      type: STORE_PROPERTY,
      property,
      images,
      amenities,
    });
