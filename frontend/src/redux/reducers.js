import {
  CREATE_PROPERTY,
  GET_ALL_PROPERTIES,
  GET_PROPERTY,
  GET_PROPERTY_AMENITIES,
  GET_PROPERTY_IMAGES,
  STORE_PROPERTY,
} from "./actions";

const createPropertyFormInitialState = {
  isSubmitting: false,
  formValues: {
    property: {
      publicKey: "",
      name: "",
      description: "",
      price: 0.0,
      propertyType: "",
      operation: "",
      state: "",
      city: "",
      neighborhood: "",
      cp: "",
      street: "",
      latitude: "",
      longitude: "",
      numBathrooms: 0,
      bedrooms: 0,
      m2Construction: 0,
      parking: 0,
      age: 0,
      departments: 0,
      floor: 0,
    },
    images: [],
    amenities: [],
  },
};

export const propertyReducer = (state = {}, action) => {
  switch (action.type) {
    case GET_ALL_PROPERTIES:
      return {
        ...state,
        properties: action.properties,
      };
    case GET_PROPERTY:
      return {
        ...state,
        currentProperty: action.property,
      };
    case GET_PROPERTY_IMAGES:
      return {
        ...state,
        currentPropertyImages: action.images,
      };
    case GET_PROPERTY_AMENITIES:
      return {
        ...state,
        currentPropertyAmenities: action.amenities,
      };
    default:
      return state;
  }
};

export const createPropertyFormReducer = (
  state = createPropertyFormInitialState,
  action
) => {
  console.log('reducer', action);
  switch (action.type) {
    case CREATE_PROPERTY:
      return {
        ...state,
      };
    case STORE_PROPERTY:
      return {
        ...state,
        formValues: {
          ...state.formValues,
          property: action.property,
          images: action.images ?? [],
          amenities: action.amenities ?? [],
        },
      };
    default:
      return state;
  }
};
