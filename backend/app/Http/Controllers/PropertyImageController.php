<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\PropertyImage;

class PropertyImageController extends Controller
{
    public function index(Property $property){
        $propertyImages = PropertyImage::where('property', $property->id)->get();

        return $propertyImages;
    }
}
