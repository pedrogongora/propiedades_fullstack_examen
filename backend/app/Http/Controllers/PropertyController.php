<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Amenity;
use App\Models\Property;
use App\Models\PropertyImage;


use Illuminate\Support\Facades\Storage;

class PropertyController extends Controller
{
    public function index()
    {
        $propeties = Property::with(['images'])->get();

        return $propeties;
    }

    public function mvcProperties()
    {
        $propeties = Property::all();
        return view('properties',compact('propeties'));
    }
    
    public function show(Property $property)
    {
        return $property;
    }

    public function store(Request $request)
    {
        error_log('store');
        $input = $request->input();
        $payload = json_decode(json_encode($input));
        $property = new Property;
        $property->public_key = $payload->property->publicKey;
        $property->name = $payload->property->name;
        $property->description = $payload->property->description;
        $property->price = $payload->property->price;
        $property->property_type = $payload->property->propertyType;
        $property->operation = $payload->property->operation;
        $property->state = $payload->property->state;
        $property->city = $payload->property->city;
        $property->neighborhood = $payload->property->neighborhood;
        $property->cp = $payload->property->cp;
        $property->street = $payload->property->street;
        $property->latitude = $payload->property->latitude;
        $property->longitude = $payload->property->longitude;
        $property->num_bathrooms = $payload->property->numBathrooms;
        $property->bedrooms = $payload->property->bedrooms;
        $property->m2_construction = $payload->property->m2Construction;
        $property->parking = $payload->property->parking;
        $property->age = $payload->property->age;
        $property->departments = $payload->property->departments;
        $property->floor = $payload->property->floor;
        $property->user = 1;
        error_log('storing');
        $res = $property->save();
        error_log('stored');
        error_log('storing amenities');
        $amenities = [];
        foreach($payload->amenities as $index=>$amenity_id)
        {
            $amenity = Amenity::find($amenity_id);
            array_push($amenities, $amenity);
        }
        $property->amenities()->saveMany($amenities);
        error_log('stored amenities');
        // TODO
        $image = new PropertyImage;
        $image->path = 'https://propiedadescom.s3.amazonaws.com/files/600x400/amsterdam-46-hipodromo-cuauhtemoc-df-cdmx-24201393-foto-01.jpg';
        $image->property = $property->id;
        $image->order = 1;
        $image->save();
        error_log('stored image');
    }
}
