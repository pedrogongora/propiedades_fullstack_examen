<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Property;
use App\Models\Amenity;

class PropertyAmenityController extends Controller
{
    public function index($propertyId){
        $propertyAmenities = Property::with(['amenities'])->find($propertyId)->amenities;

        return $propertyAmenities;
    }
}
