<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('properties', 'App\Http\Controllers\PropertyController@index');
Route::post('properties', 'App\Http\Controllers\PropertyController@store');
Route::get('properties/property/{property}', 'App\Http\Controllers\PropertyController@show');
Route::get('properties/images/{property}', 'App\Http\Controllers\PropertyImageController@index');
Route::get('properties/amenities/{property}', 'App\Http\Controllers\PropertyAmenityController@index');

Route::get('amenities', 'App\Http\Controllers\AmenityController@index');
